from django.db import models

from apps.model.ArticleModel import Article


class Magazine(models.Model):

    article         =    models.ForeignKey(Article, on_delete=models.CASCADE, unique=False)
    Magazine_name   =    models.CharField(max_length=255)
    publisher       =    models.CharField(max_length=255)
    publish_date    =    models.DateTimeField(max_length=32, null=True)
    edition         =    models.CharField(max_length=64)
    created_at      =    models.DateTimeField(auto_now_add=True)
    updated_at      =    models.DateTimeField(auto_now=True, null=True)

    objects = models.Manager()


    class Meta:
        db_table = "magazine"