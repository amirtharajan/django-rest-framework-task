from django.db import models

from apps.models import Login



class Author(models.Model):

    login           =    models.OneToOneField(Login, on_delete=models.CASCADE, null=True, unique=True)
    first_name      =    models.CharField(max_length=255)
    last_name       =    models.CharField(max_length=255, null=True)
    gender          =    models.CharField(max_length=32, null=True)
    mobile          =    models.CharField(max_length=32, null=True)
    created_at      =    models.DateTimeField(auto_now_add=True)
    updated_at      =    models.DateTimeField(auto_now=True, null=True)

    objects = models.Manager()


    class Meta:
        db_table = "authors"