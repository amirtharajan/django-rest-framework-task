from django.db import models

from apps.model.AuthorModel import Author



class Article(models.Model):

    author      =   models.OneToOneField(Author, on_delete=models.CASCADE, unique=False, null=False, blank=False)
    title       =   models.CharField(max_length=128, blank=False)
    content     =   models.TextField(null=True)
    created_at  =   models.DateTimeField(auto_now_add=True)
    updated_at  =   models.DateTimeField(auto_now=True, null=True)

    objects = models.Manager()


    class Meta:
        db_table = "articles"