from .ArticleModel import Article
from .AuthorModel import Author
from .MagazineModel import Magazine
from .LoginHistoryModel import LoginHistory