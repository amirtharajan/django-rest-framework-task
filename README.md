# Django Rest Framework Task


# Django Rest Framework Task

To access this application, First you need to register and login
Than provide the Auth Token to access the end points

Steps:
	1. Register
	2. Login (Now you can get the Token)
	3. Goto Headers in the POSTMAN, Enable the 'Authorization' key and assign the Token as a key with prefix of 'token ' 
	   (ex : 'token 615ccc659b799c229c39e382a28fa249aa2df7ec')
	4. Done, Now you can access other endpoints.

---------------------------------------------------------------------------------------------------------------

If you want run this application from your machine, please execute the following 7 steps.

	Step 1 : Download & Install MySQL Server

	Step 2 : Download & Install Python3

	Step 3 : Clone the project from the GitLab

	Step 4 : Ensure “.env” file in your cloned project folder

	Step 5 : Ensure “requirements.txt” file in your cloned project folder

	Step 6 : Create new database in your MySQL named “rest_database”
		(Recommend:  Download HEIDISQL to manage your MySQL server in User Friendly from the following link)
		https://www.heidisql.com/download.php

	Step 7 : Run the following command from your cloned project folder
		pip install -r requirements.txt
		python manage.py makemigrations
		python manage.py migrate   
		python manage.py runserver

----------------------------------------------- *** THANK YOU *** ---------------------------------------------
